﻿using System;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
           
        }


        public int Task_1_A(int[] array)
        {
            throw new NotImplementedException();
        }

        public int Task_1_B(int[] array)
        {
            throw new NotImplementedException();
        }

        public int Task_1_C(int[] array)
        {
            throw new NotImplementedException();
        }

        public int Task_1_D(int[] array)
        {
            throw new NotImplementedException();
        }

        public void Task_1_E(int[] array)
        {
            throw new NotImplementedException();
        }

        public int Task_2(int a, int b, int c)
        {
            throw new NotImplementedException();
        }

        public int Task_2(int a, int b)
        {
            throw new NotImplementedException();
        }

        public double Task_2(double a, double b, double c)
        {
            throw new NotImplementedException();
        }

        public string Task_2(string a, string b)
        {
            throw new NotImplementedException();
        }

        public int[] Task_2(int[] a, int[] b)
        {
            throw new NotImplementedException();
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            throw new NotImplementedException();
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            throw new NotImplementedException();
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            throw new NotImplementedException();
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            throw new NotImplementedException();
        }

        public (double, double) Task_4_B(double radius)
        {
            throw new NotImplementedException();
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            throw new NotImplementedException();
        }

        public void Task_5(int[] array)
        {
            throw new NotImplementedException();
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            throw new NotImplementedException();

        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            throw new NotImplementedException();
        }
    }
}
